#!/bin/sh

#
# Helps generate autoconf/automake stuff, when code is checked out from SCM.
#
# Copyright (C) 2006-2010 - Karel Zak <kzak@redhat.com>
#

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

THEDIR=`pwd`
cd $srcdir
DIE=0

warn_mesg ()
{
	echo
	echo "WARNING: $1"
	test -z "$2" ||
		echo "       $2"
	echo
}

error_mesg ()
{
	echo
	echo "ERROR: $1"
	test -z "$2" ||
		echo "       $2"
	echo
	DIE=1
}

test -f sys-utils/mount.c ||
	error_mesg "You must run this script in the top-level util-linux directory."

(autoconf --version) < /dev/null > /dev/null 2>&1 ||
	error_mesg "You must have autoconf installed to generate the util-linux build system."

(autoheader --version) < /dev/null > /dev/null 2>&1 ||
	error_mesg "You must have autoheader installed to generate the util-linux build system."  "The autoheader command is part of the GNU autoconf package."

if ! (bison --version) < /dev/null > /dev/null 2>&1; then
	error_mesg "You must have bison installed to build the util-linux."
else
	lexver=$(bison --version | awk '/^bison \(GNU [Bb]ison\)/ { print $4 }')
	case "$lexver" in
		[2-9].*)
			;;
		*)
			error_mesg "You must have bison version >= 2.x, but you have $lexver."
			;;
	esac
fi

(automake --version) < /dev/null > /dev/null 2>&1 ||
	error_mesg "You must have automake installed to generate the util-linux build system."

if test "$DIE" -eq 1; then
	exit 1
fi

echo
echo "Generating build-system with:"
echo "   aclocal:    $(aclocal --version | head -1)"
echo "   autoconf:   $(autoconf --version | head -1)"
echo "   autoheader: $(autoheader --version | head -1)"
echo "   automake:   $(automake --version | head -1)"
echo "   bison:      $(bison --version | head -1)"
echo

rm -rf autom4te.cache

set -e

aclocal -I m4 $AL_OPTS
autoconf $AC_OPTS
autoheader $AH_OPTS

automake --add-missing $AM_OPTS


cd "$THEDIR"

echo
echo "Now type '$srcdir/configure' and 'make' to compile."
echo


